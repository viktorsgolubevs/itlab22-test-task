import React from 'react';
import PropTypes from 'prop-types';

export default class Container extends React.Component {

  render() {

    return (
      <div className={`container ${this.props.cssClass ? this.props.cssClass : ''}`}>
        {this.props.children}
      </div>
    );
  }
}

Container.propTypes = {
  cssClass: PropTypes.string,
  children: PropTypes.object,
};
