import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

const Form = props => {

  const [user, setUser] = useState(props.userData);

  const [error, setError] = useState(0);

  useEffect(
    () => {
      setUser(props.userData);
      setError(0)
    },
    [props],
  );

  const changeInputHandler = event => {

    const { name, value } = event.target;

    setUser({
      ...user,
      [name]: value,
    });
  };

  const handleValidation = () => {
    const fields = user;
    const errors = {};
    let formIsValid = true;

    if (!fields['name']) {
      formIsValid = false;
      errors['name'] = 'Cannot be empty';
    }

    if (!fields['surname']) {
      formIsValid = false;
      errors['surname'] = 'Cannot be empty';
    }


    if (typeof fields['name'] !== 'undefined') {
      if (!fields['name'].match(/^[a-zA-Z]+$/)) {
        formIsValid = false;
        errors['name'] = 'Only letters';
      }
    }

    if (typeof fields['surname'] !== 'undefined') {
      if (!fields['surname'].match(/^[a-zA-Z]+$/)) {
        formIsValid = false;
        errors['surname'] = 'Only letters';
      }
    }

    if (fields['age']) {
      if (!fields['age'].match(/^[0-9]+$/)) {
        formIsValid = false;
        errors['age'] = 'Only numbers';
      }
    }

    setError(errors);
    return formIsValid;
  };

  const handleSubmit = event => {

    event.preventDefault();

    if (handleValidation()) {
      props.handleSubmit(user);
    }

  };

  const submitButtonName = props.editMode ? 'Edit' : 'Add';

  return (
    <form className="form__wrap" onSubmit={handleSubmit}>
      <div className="form__field-group">
        <input type="text" className={`form__input + ${error.name ? 'form__input--error' : ''}`} name="name" placeholder="Name" value={user.name}
               onChange={changeInputHandler} onFocus={(e) => e.target.placeholder = ''}
               onBlur={(e) => e.target.placeholder = 'Name'}/>
        <span className={`form__field-error + ${error.name ? 'form__field-error--show' : ''}`}>{error.name}</span>
      </div>

      <div className="form__field-group">
        <input type="text" className={`form__input + ${error.surname ? 'form__input--error' : ''}`} name="surname" placeholder="Surname" value={user.surname}
               onChange={changeInputHandler} onFocus={(e) => e.target.placeholder = ''}
               onBlur={(e) => e.target.placeholder = 'Surname'}/>
        <span className={`form__field-error + ${error.surname ? 'form__field-error--show' : ''}`}>{error.surname}</span>
      </div>

      <div className="form__field-group">
        <input type="text" className={`form__input form__input--hide-placeholder + ${error.age ? 'form__input--error' : ''}`} name="age" value={user.age}
               onChange={changeInputHandler} placeholder="Age"/>
        <span className={`form__field-error + ${error.age ? 'form__field-error--show' : ''}`}>{error.age}</span>
      </div>

      <div className="form__field-group">
        <input type="text" className={`form__input form__input--hide-placeholder + ${error.city ? 'form__input--error' : ''}`} name="city" value={user.city}
               onChange={changeInputHandler} placeholder="City"/>
        <span className={`form__field-error + ${error.city ? 'form__field-error--show' : ''}`}>{error.city}</span>
      </div>

      <div className="form__field-group--submit-button">
        <button className="button__form button__form--submit button__form--full-width button__form--uppercase"
                onClick={handleSubmit}>{submitButtonName}</button>
      </div>
    </form>
  );
};

Form.propTypes = {
  userData: PropTypes.object,
  editMode: PropTypes.object,
  handleSubmit: PropTypes.func,
};

export default Form;
