import React from 'react'
import PropTypes from 'prop-types'

class TableNavigation extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {

    return (
      <div className="table__navigation">
        <ul>
          <li>
            <a href="#" className="button__table button__table--copy"
                 onClick={(e) => this.props.handleCopyTable(e, this.props.table)}>Copy table</a>
          </li>
          {this.props.table !== 0 ?
            <li>
              <a href="#" className="button__table--remove" onClick={(e) => this.props.handleDeleteTable(e, this.props.table)}>&nbsp</a>
            </li>
            : null}
        </ul>
      </div>
    )
  }

}

TableNavigation.propTypes = {
  table: PropTypes.number,
  handleCopyTable: PropTypes.func,
  handleDeleteTable: PropTypes.func,
}

export default TableNavigation
