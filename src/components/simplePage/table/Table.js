import React from 'react'
import PropTypes from 'prop-types'
import TableNavigation from './TableNavigation'

class Table extends React.Component {

  render() {

    const tableData = this.props.tableData ? this.props.tableData : []

    return (

      tableData.map((item, tableIndex) => {
        return (
          <React.Fragment key={tableIndex}>
            <TableNavigation table={tableIndex} handleCopyTable={this.props.handleCopyTable}
                             handleDeleteTable={this.props.handleDeleteTable}/>
            <div className="table">

              <table className="table__wrap">
                <thead>
                <tr>
                  <th className="table__cell table__cell--thead">Name</th>
                  <th className="table__cell table__cell--thead">Surname</th>
                  <th className="table__cell table__cell--thead">Age</th>
                  <th className="table__cell table__cell--thead">City</th>
                  <th className="table__cell table__cell--thead"></th>
                </tr>
                </thead>
                <tbody>
                {
                  tableData[tableIndex].length ? (

                    tableData[tableIndex].map((item, rowIndex) => {
                      return (
                        <tr key={rowIndex}>
                          <td className="table__cell">{item.name}</td>
                          <td className="table__cell">{item.surname}</td>
                          <td className="table__cell">{item.age}</td>
                          <td className="table__cell">{item.city}</td>
                          <td className="table__cell">
                            <div className="table__cell-actions">
                              <a href="#" className="table__link table__link--blue"
                                 onClick={(e) => this.props.handleEditTableRow(e, tableIndex, rowIndex)}>Edit</a>
                              <a href="#" className="table__link table__link--red"
                                 onClick={(e) => this.props.handleDeleteTableRow(e, tableIndex, rowIndex)}>Delete</a>
                            </div>
                          </td>
                        </tr>
                      )
                    })

                  ) : (

                    <tr>
                      <td colSpan={5} className="table__cell table__cell--no-table-data">No data</td>
                    </tr>

                  )
                }
                </tbody>
              </table>
            </div>
          </React.Fragment>
        )
      })
    )
  }

}

Table.propTypes = {
  tableData: PropTypes.array,
  handleEditTableRow: PropTypes.func,
  handleDeleteTableRow: PropTypes.func,
}

export default Table
