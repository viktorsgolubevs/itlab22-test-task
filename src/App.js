import React from 'react'

import Form from './components/simplePage/form/Form'
import Table from './components/simplePage/table/Table'
import Container from './components/layout/Container'

class App extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      editMode: null,
      table: [],
      userData: this.initialState(),
    }
  }

  initialState = () => ({
    name: '',
    surname: '',
    age: '',
    city: '',
  })

  handleForm = (userData) => {

    const { editMode, table } = this.state

    if (editMode) {

      table[editMode.tableId][editMode.tableRowId] = userData

      this.setState(prev => (
        {
          ...prev, ...{
            editMode: null,
            table: table,
            userData: this.initialState(),
          },
        }
      ))

    } else {

      if (!table.length) {
        table.push([])
      }

      table[0].push(userData)

      this.setState(prev => (
        { ...prev, ...{ table: table } }
      ))

    }

  }

  handleEditTableRow = (event, tableId, tableRowId) => {

    event.preventDefault()

    if (typeof tableId === 'undefined' && typeof tableRowId === 'undefined') {
      return
    }

    const { table } = this.state

    const user = table[tableId][tableRowId]

    this.setState({
      editMode: {
        tableId,
        tableRowId,
      },
      userData: user,
    })

  }

  handleDeleteTableRow = (event, tableId, tableRowId) => {

    event.preventDefault()

    if (typeof tableId === 'undefined' && typeof tableRowId === 'undefined') {
      return
    }

    const { table } = this.state

    table[tableId].splice(tableRowId, 1)

    this.setState(prev => (
      { ...prev, ...{ table: table } }
    ))

  }

  handleCopyTable = (event, tableId) => {

    event.preventDefault()

    if (typeof tableId === 'undefined') {
      return
    }

    const { table } = this.state

    //const defaultTable = JSON.parse(JSON.stringify(table[tableId])) Old school variant
    const defaultTable = [...table[tableId]]

    table.push(defaultTable)

    this.setState(prev => (
      { ...prev, ...{ table: table } }
    ))

  }

  handleDeleteTable = (event, tableId) => {

    event.preventDefault()

    if (typeof tableId === 'undefined') {
      return
    }

    const { table } = this.state

    table.splice(tableId, 1)

    this.setState(prev => (
      { ...prev, ...{ table: table } }
    ))

  }

  render() {

    const { table, editMode, userData } = this.state

    return (
      <div className="app">
        <Container>
          <div className="simple-page">
            <div className="simple-page__form-wrap simple-page__form-wrap--half-width">
              <Form
                userData={userData}
                editMode={editMode}
                handleSubmit={this.handleForm}
              />
            </div>
            <div className="simple-page__table-wrap">

              <Table
                tableData={table}
                handleEditTableRow={this.handleEditTableRow}
                handleDeleteTableRow={this.handleDeleteTableRow}
                handleCopyTable={this.handleCopyTable}
                handleDeleteTable={this.handleDeleteTable}
              />
            </div>
          </div>
        </Container>
      </div>
    )
  }
}

export default App
